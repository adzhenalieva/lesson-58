import React, {Component} from 'react';
import Modal from "./component/Modal/Modal";
import Alert from "./component/Alert/Alert";
import MainPage from "./component/MainPage/MainPage";

import './App.css';
import Button from "./component/Button/Button";


class App extends Component {

    closeModal = () => {
        this.setState({showModal: false})
    };

    continued = () => {
        alert('You continued modal window');
    };

    state = {
        showModal: false,
        showAlert: false,
        showClose: false,
        alert: ["alert-success", "alert-danger", 'alert-warning'],
        modal: [
            {type: 'primary', label: 'Continue', clicked: this.continued},
            {type: 'danger', label: 'Close', clicked: this.closeModal}
        ]

    };

    showModal = () => {
        this.setState({showModal: true})
    };

    showButtons = () => {
        let buttons = [...this.state.modal];
        return buttons.map((button, id) => (
            <Button key={id}
                    type={button.type}
                    className={['btn', button.type].join(' btn-')}
                    onClick={button.clicked}
            >{button.label}</Button>
        ))
    };
    showCloseButton = () => {
        this.setState({showClose: true})
    };
    showAlert = () => {
        this.setState({showAlert: true})
    };

    closeAlert = () => {
        this.setState({showAlert: false})
    };

    render() {
        return (
            <div className="App container">
                <MainPage>
                    <button className="btn mr-4" onClick={this.showModal}>Show modal</button>
                    <button className="btn" onClick={this.showAlert}>Show Alert</button>
                    <Modal show={this.state.showModal}
                           onClick={this.closeModal}
                           title="Modal window title">
                        {this.showButtons()}
                    </Modal>
                    <Alert
                        show={this.state.showAlert}
                        onClick={this.closeAlert}
                        type={this.state.alert[0]}
                        showClose={this.showCloseButton}
                    >Alert text</Alert>
                    <Alert
                        show={this.state.showAlert}
                        onClick={this.closeAlert}
                        type={this.state.alert[1]}
                        showClose={this.state.showClose}
                    >Alert text 2</Alert>
                    <Alert
                        show={this.state.showAlert}
                        onClick={this.closeAlert}
                        type={this.state.alert[2]}
                        showClose={this.showCloseButton}
                    >Alert text 3</Alert>
                </MainPage>
            </div>
        );
    }
}

export default App;
