import React, {Fragment} from 'react';

import "./Modal.css";
import Backdrop from "../Backdrop/Backdrop";


const Modal = props => {
    return (
        <Fragment>
            <Backdrop show={props.show} onClick={props.onClick}/>
            <div className="Modal modal-content" style={
                {
                    transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                    opacity: props.show ? '1' : '0'
                }
            }>

                <h3 className="modal-title">
                    {props.title}
                    <button className="btn btn-secondary ml-5" onClick={props.onClick}>X</button>
                </h3>
                <div className="modal-body">
                    <p>Modal window text</p>
                </div>
                <div className="modal-footer">

                    {props.children}
                </div>

            </div>
        </Fragment>
    );
};

export default Modal;