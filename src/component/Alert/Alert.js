import React from 'react';
import "./Alert.css"

const Alert = props => {
    return (
        <div className={['alert-dismissible','alert', props.type].join(' ')}
             style={
                 {
                     display: props.show ? 'block' : 'none'
                 }}>
            <button className="close"  style={
                {
                    display: props.showClose ? 'block' : 'none'
                }} onClick={props.onClick}><span>&times;</span></button>
            {props.children}
        </div>
    );
};

export default Alert;